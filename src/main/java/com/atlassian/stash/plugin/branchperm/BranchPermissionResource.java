package com.atlassian.stash.plugin.branchperm;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.hooks.permissions.BranchPermissionService;
import com.atlassian.stash.repository.*;
import com.atlassian.stash.rest.util.ResourcePatterns;
import com.atlassian.stash.rest.util.RestUtils;
import com.atlassian.stash.util.PageRequestImpl;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Set;

import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;

@Singleton
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@AnonymousAllowed
@Path(ResourcePatterns.REPOSITORY_URI)
public class BranchPermissionResource {

    private final BranchPermissionService branchPermissionService;
    private final RepositoryMetadataService repositoryMetadataService;

    public BranchPermissionResource(BranchPermissionService branchPermissionService, RepositoryMetadataService repositoryMetadataService) {
        this.branchPermissionService = branchPermissionService;
        this.repositoryMetadataService = repositoryMetadataService;
    }

    @GET
    public Response getAllBranches(@Context final Repository repository, @QueryParam("max") @DefaultValue("1000") int max) throws IOException {
        Iterable<String> branches = Iterables.transform(repositoryMetadataService.getBranches(new RepositoryBranchesRequest.Builder()
                .repository(repository)
                .build(), new PageRequestImpl(0, max)).getValues(), MinimalRef.TO_ID);
        Set<String> unauthorizedRefs = ImmutableSet.copyOf(branchPermissionService.findUnauthorizedRefs(repository, branches));
        return Response.ok(ImmutableMap.of("branches", filter(branches, not(in(unauthorizedRefs))))).build();
    }

    @GET
    @Path("/{branch}")
    public Response get(@Context final Repository repository, @PathParam("branch") String branch) throws IOException {
        Ref ref = repositoryMetadataService.resolveRef(repository, branch);
        if (ref == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        boolean allowed = Iterables.isEmpty(branchPermissionService.findUnauthorizedRefs(repository, ImmutableList.of(ref.getId())));
        return allowed ? Response.ok().build() : Response.status(Response.Status.FORBIDDEN).build();
    }

}
