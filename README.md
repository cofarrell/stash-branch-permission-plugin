# Stash Branch Permission Plugin

This plugin exposes the branch permission for a user over REST:

    curl -v -u user:pass -X GET -H "Content-Type:application/json" "http://host:port/rest/branch-permission/1.0/projects/$PROJECT/repos/$REPO?max=1000"

And for a specific branch:

    curl -v -u user:pass -X GET "http://host:port/rest/branch-permission/1.0/projects/$PROJECT/repos/$REPO/$BRANCH"

With the expected return codes:

- 200: Permisssion
- 403: No permission
- 404: Not Found
